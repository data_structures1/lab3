public class SearchinRotatedSortedArray {
    public static void main(String[] args){
        int nums1[] = {4, 5, 6, 7, 0, 1, 2};
        int nums2[] = {1};

        int t1 = 0;
        int t2 = 3;
        int t3 = 0;

        showArr(nums1,t1);
        int case1 = SearchIndex(nums1,t1);
        System.out.println(case1);

        showArr(nums1,t2);
        int case2 = SearchIndex(nums1,t2);
        System.out.println(case2);

        showArr(nums2,t3);
        int case3 = SearchIndex(nums2,t3);
        System.out.println(case3);

    }

    private static void showArr(int[] arr, int target) {
        System.out.print("nums = [");
        for(int i =0, comma = 1;i<arr.length;i++){
            System.out.print(arr[i]);
            if(comma<arr.length){
                System.out.print(",");
            }
            comma++;

        }
        System.out.print("]");
        System.out.println(" target = "+target);

    }

    private static int SearchIndex(int arr[], int target) {
        int result = -1;
        for(int i=0; i<arr.length; i++){
            if(target == arr[i]){
                result = i;
                return result;
            }
        }
        return result;
    }
}
